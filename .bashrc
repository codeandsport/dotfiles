#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
source ~/.myalias
PS1='[\u@\h \W]\$ '

[ -f ~/.fzf.bash ] && source ~/.fzf.bash
bind "set completion-ignore-case on"
bind "set show-all-if-ambiguous on"
export THEME=$HOME/.bash/themes/agnoster-bash/agnoster.bash
if [[ -f $THEME ]]; then
    export DEFAULT_USER=`whoami`
    source $THEME
fi
export PATH=$PATH:/$HOME/.config/composer/vendor/bin
PATH="$PATH:$(ruby -e 'print Gem.user_dir')/bin"
export GEM_HOME=$HOME/.gem
#[[ $TERM == *"rxvt"* ]] && wmctrl -r :ACTIVE: -b add,fullscreen

bind -x '"\C-g": fg'
